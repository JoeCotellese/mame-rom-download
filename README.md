# Mame Rom Download

## Description

I use MAME on the Mac and all of the tools for managing rom sets are unavailable. Dealing with merged / unmerged sets is a pain. Especially because I only really care about a handful of games. 

So, this little Python script will take a list of games in config.yml and download those ROMS from the Internet archive.

## Requirements

- pipenv
- requests
- pyyaml

## Installation

Clone the repo.

`pipenv install`

## Running
Edit config.yml and add your games and roms. 
then `pipenv run python mame-rom-download.py`

## Other
PRs welcome

