"""
mame-rom-download.py

This script is used to download MAME ROMs and artwork from specified URLs.
It reads a list of game names from a YAML configuration file and downloads
the corresponding ROM and artwork files. If the files already exist, they
are not downloaded again.

Usage:
    poetry run python mame-rom-download.py --force [optional game name]

    Options:
    --force: Force download of all files, even if they already exist.

    Example:
    poetry run python mame-rom-download.py --force
    poetry run python mame-rom-download.py --force "1942"

"""
import os

import click
import requests
import yaml

rom_url = 'https://archive.org/download/mame-merged/mame-merged/'
rom_path = '/Volumes/External/MAME/roms/'

# artwork_url = 'https://mrdo.mameworld.info/artwork/'
artwork_url = 'https://www.progettosnaps.net/artworks/packs/official/'
artwork_path = '/Volumes/External/MAME/artwork/'


def download_file(name, base_url, download_path):
    print('Downloading: ' + name)
    file_name = name + '.zip'
    url = base_url + file_name
    # full_download_path = download_path + file_name
    r = requests.get(url, allow_redirects=True)
    open(download_path + file_name, 'wb').write(r.content)


def get_full_download_path(name, download_path):
    return download_path + name + '.zip'


def download_rom(name, base_url, force=False):
    if force:
        download_file(name, base_url, rom_path)
        return
    if not os.path.exists(get_full_download_path(name, rom_path)):
        download_file(name, base_url, rom_path)


def download_artwork(name, base_url, force=False):
    if force:
        download_file(name, base_url, artwork_path)
        return
    if not os.path.exists(get_full_download_path(name, artwork_path)):
        download_file(name, base_url, artwork_path)


def check_paths():
    if not os.path.exists(rom_path):
        print('Creating path: ' + rom_path)
        os.makedirs(rom_path)

@click.command()
@click.option('--force', is_flag=True, 
              help='Force download of all files, even if they already exist.')
@click.argument('game_name', required=False)
def download(force, game_name=None):
    # read YAML configuration file
    if game_name:
        download_rom(game_name, rom_url, force)
        download_artwork(game_name, artwork_url)
        return
    
    with open('config.yml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
        # loop over the list of games
        for game_name in data['games']:
            # download game
            download_rom(str(game_name), rom_url, force)
            # download artwork
            download_artwork(str(game_name), artwork_url)
        
        # loop over the list of supplemental roms
        for rom_name in data['roms']:
            # download roms
            download_rom(rom_name, rom_url)

if __name__ == '__main__':
    # check_paths()
    download()


# open games.lst and read each line
# with open('games.txt') as f:
#     for line in f:
#         # remove new line character
#         line = line.rstrip()
#         # split line into array
#         line_array = line.split(' ')
#         # get game name
#         game_name = line_array[0]
#         # download game
#         download_rom(game_name, rom_url)
#         # download artwork
#         download_artwork(game_name, artwork_url)

